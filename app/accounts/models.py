import uuid
import datetime


from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin


from projects.models import ProjectViewLimits


from .managers import AccountManager


class Account(AbstractBaseUser, PermissionsMixin):
    full_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    def __str__(self):
        return self.full_name

    def get_tokens_count(self):
        return self.tokens.filter(is_used=False).count()

    def zero_tokens(self):
        return self.get_tokens_count() == 0




class AccountVoteToken(models.Model):
    account = models.ForeignKey(Account, related_name='tokens', on_delete=models.CASCADE)
    value = models.CharField(max_length=255)
    is_used = models.BooleanField(default=False)


@receiver(post_save, sender=Account)
def generate_tokens(sender, **kwargs):
    if kwargs.get('created'):
        instance = kwargs.get('instance')
        AccountVoteToken.objects.bulk_create([
            AccountVoteToken(account=instance, value="{}{}".format(x, uuid.uuid4().hex)) for x in range(15)
        ])
