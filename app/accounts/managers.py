# -*- coding: utf-8 -*-
import datetime

from django.contrib.auth.models import BaseUserManager


class AccountManager(BaseUserManager):

    def create_user(self, full_name, email, password=None, is_active=True, is_admin=False):

        if not email:
            raise ValueError(u'Email is required')

        user = self.model(
            full_name=full_name,
            email=self.normalize_email(email),
            is_active=is_active,
            is_admin=is_admin,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, full_name, email, password):

        user = self.create_user(
            full_name=full_name,
            email=email,
            password=password,
            is_active=True,
            is_admin=True,
        )
        user.save(using=self._db)
        return user
