# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand


from accounts.models import Account
from projects.models import Project


class Command(BaseCommand):
    help = """
    Generates test data.
    All users has passwords Panda#123
    """

    def handle(self, *args, **options):
        for id in range(5):
            account = Account.objects.create_user(
                full_name='User #{}'.format(id),
                email="user{}@starmail.com".format(id),
                password='Panda#123',
                is_active=True,
                is_admin=False
            )

            Project.objects.bulk_create([
                Project(user=account, description="{}".format(x), title='{} story #{}'.format(account.full_name, x)) for x in range(10)
            ])

