import uuid


from django.views.generic import FormView, CreateView
from django.urls import reverse
from django.contrib.auth import login, authenticate
from django.shortcuts import redirect

from .forms import LoginForm, RegistrationForm
from .models import Account, AccountVoteToken


class HomePageView(FormView):
    template_name = 'index.html'
    form_class = LoginForm

    def get_success_url(self):
        return reverse('home')

    def dispatch(self, request, *args, **kwargs):
        if self.request.user and self.request.user.is_authenticated:
            return redirect(self.get_success_url())
        return super(HomePageView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(HomePageView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['button_label'] = 'Login'
        return context


class RegistrationView(CreateView):
    template_name = 'index.html'
    model = Account
    form_class = RegistrationForm

    def get_context_data(self, **kwargs):
        context = super(RegistrationView, self).get_context_data(**kwargs)
        context['button_label'] = 'Register'
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = True
        self.object.save()
        user = self.object
        AccountVoteToken.objects.bulk_create([
            AccountVoteToken(account=user, value="{}{}".format(x, uuid.uuid4().hex)) for x in range(15)
        ])
        authenticated = authenticate(
            username=user.email,
            password=self.request.POST.get('password1')
        )
        if authenticated:
            login(self.request, authenticated)
            return redirect(reverse('home'))
        return super(RegistrationView, self).form_valid(form)

    def get_success_url(self):
        return reverse('home')
