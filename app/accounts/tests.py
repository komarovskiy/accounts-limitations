from django.test import TestCase, RequestFactory
from django.urls import resolve, reverse
from django.contrib.auth.models import AnonymousUser

from .models import Account
from .forms import RegistrationForm
from projects.views import ProjectsView


class AccountsTest(TestCase):
    fixtures = ['app']

    def setUp(self):
        self.user = Account.objects.create_user(full_name='Name',
                                                email='newemail@starmail.com',
                                                password='Password#123', is_active=True)

    def test_url_resolves_to_login_form_page(self):
        found = resolve('/')
        self.assertEqual(found.func.view_class, ProjectsView)

    def test_uses_index_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'ratings.html')

    def test_users_installed_from_features(self):
        users = Account.objects.all()
        self.assertEqual(users.count(), 7)
        self.assertEqual(self.user.get_tokens_count(), 15)

    def test_user_login(self):
        self.client.login(username='newemail@starmail.com', password='Password#123')
        response = self.client.get('/projects/add/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def test_anonymous_ratings_page(self):
        self.user = AnonymousUser()
        response = self.client.get('/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_registration_form(self):

        invalid_data_dicts = [
            {
                'data':
                    {'full_name': 'Full Name',
                     'email': 'foo@foo/bar.com',
                     'password1': 'foo',
                     'password2': 'foo'},
                'error':
                    ('email', ["Enter a valid email address."])
            },
            {
                'data':
                    {'full_name': 'Already',
                     'email': 'newemail@starmail.com',
                     'password1': 'secret',
                     'password2': 'secret'},
                'error':
                    ('email', ["User with this email already exists."])
            },
            {
                'data':
                    {'full_name': 'foo',
                     'email': 'foo@example.com',
                     'password1': 'fooFiii#123',
                     'password2': 'fooFiii#124'},
                'error':
                    ('password2', [u"Passwords don`t match"])
            },
        ]

        for invalid_dict in invalid_data_dicts:
            form = RegistrationForm(data=invalid_dict['data'])
            self.failIf(form.is_valid())
            self.assertEqual(form.errors[invalid_dict['error'][0]], invalid_dict['error'][1])

        form = RegistrationForm(data={'full_name': 'foo',
                                            'email': 'foo@example.com',
                                            'password1': 'fooFiii#123',
                                            'password2': 'fooFiii#123'})
        self.failUnless(form.is_valid())
        form.save()
        self.assertEqual(Account.objects.count(), 8)

    def test_registration_view(self):
        response = self.client.post(reverse('register'),
                                    data={'full_name': 'Who?',
                                          'email': 'newemail@starmail.com',
                                          'password1': 'fooFiii#123',
                                          'password2': 'fooFiii#123'})
        self.assertEqual(response.status_code, 200)
        self.failUnless(response.context['form'])
        self.failUnless(response.context['form'].errors)

        response = self.client.post(reverse('register'),
                                    data={'full_name': 'Manu',
                                          'email': 'manu@ginobili.com',
                                          'password1': 'fooFiii#123',
                                          'password2': 'fooFiii#123'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], reverse('home'))
        self.assertEqual(Account.objects.count(), 8)


