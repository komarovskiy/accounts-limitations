from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'accounts'
    label = 'Account Management'
