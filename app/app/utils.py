# -*- coding: utf-8 -*-

from django.utils.html import format_html
from django.urls import reverse


import django_tables2 as tables


class ActionColumn(tables.Column):
    def __init__(self, **kwargs):
        self.action = kwargs.pop('action')
        self.label = self.action.capitalize()
        super(ActionColumn, self).__init__(**kwargs)

    def render(self, value):
        return format_html("<a href='{}'>{}</a>".format(
            reverse('{}_project'.format(self.action), kwargs={'pk': value}), self.label)
        )
