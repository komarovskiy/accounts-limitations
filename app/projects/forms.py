# -*- coding: utf-8 -*-

from django import forms


from .models import Project


class UserProjectManageForm(forms.ModelForm):
    title = forms.CharField(label='Title', max_length=255, widget=forms.TextInput(attrs={'class': "form-control"}))
    description = forms.CharField(label='Description', widget=forms.Textarea(attrs={'class': "form-control"}))

    class Meta:
        model = Project
        fields = ('title', 'description')
