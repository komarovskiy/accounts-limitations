import datetime


from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView, TemplateView
from django.urls import reverse
from django.shortcuts import redirect


from braces.views import LoginRequiredMixin


from .models import Project, ProjectViewLimits, ProjectViewsBlock
from .mixins import OwnerRequiredMixin
from .tables import OwnProjectsTable, ProjectTable
from .forms import UserProjectManageForm


class ProjectsView(TemplateView):
    template_name = 'ratings.html'

    def reorder(self, order):
        if order.startswith('-'):
           return 'title'
        return '-title'

    def get_context_data(self, **kwargs):
        projects_order = self.request.session.get('projects_order', '-title')

        self.request.session['projects_order'] = projects_order

        if self.request.GET.get('projects-sort') == 'rating':
            projects_order = self.reorder(projects_order)

        context = super(ProjectsView, self).get_context_data(**kwargs)
        projects_table = ProjectTable(Project.objects.all().order_by(projects_order), prefix='projects-')

        context['projects'] = projects_table
        return context


class UserProjectsView(ListView):
    template_name = 'projects_list.html'
    model = Project
    context_object_name = 'projects'

    def get_queryset(self):
        return OwnProjectsTable(self.request.user.projects.all())


class UserProjectsCreateView(LoginRequiredMixin, CreateView):
    template_name = 'projects_form.html'
    model = Project
    form_class = UserProjectManageForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super(UserProjectsCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('own_projects')


class UserProjectUpdateView(OwnerRequiredMixin, UpdateView):
    template_name = 'projects_form.html'
    model = Project
    form_class = UserProjectManageForm

    def get_success_url(self):
        return reverse('own_projects')


class UserProjectDeleteView(OwnerRequiredMixin, DeleteView):
    template_name = 'projects_confirm_delete.html'
    model = Project

    def get_success_url(self):
        return reverse('own_projects')


class ProjectDetailView(DetailView):
    template_name = 'projects_details.html'
    context_object_name = 'project'
    model = Project

    def dispatch(self, request, *args, **kwargs):
        if request.user.zero_tokens():
            return redirect(reverse('home'))
        obj = self.get_object()
        now = datetime.datetime.now()
        user_block = request.user.blockings.filter(created__lte=now, exceed_time__gte=now, expired=False).first()
        if not obj in user_block.projects.all() and not obj in request.user.projects.all():
            if user_block.projects.count() >= 3:
                return redirect(reverse('limit_exceed'))
            if not user_block:
                limits = ProjectViewLimits.load()
                user_block = ProjectViewsBlock()
                user_block.user = request.user
                user_block.created = now
                user_block.exceed_time = now + datetime.timedelta(minutes=limits.value)
                user_block.save()
            user_block.projects.add(obj)
            user_block.save()

        token = request.user.tokens.first()
        token.is_used = True
        token.save()

        return super(ProjectDetailView, self).dispatch(request, *args, **kwargs)


class LimitsExceedView(TemplateView):
    template_name = 'projects_limit_exceed.html'

    def get_context_data(self, **kwargs):
        context = super(LimitsExceedView, self).get_context_data(**kwargs)
        now = datetime.datetime.now()
        context['projects'] = self.request.user.blockings.filter(created__lte=now,
                                                                 exceed_time__gte=now).first().projects.all()
        return context