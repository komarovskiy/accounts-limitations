# -*- coding: utf-8 -*-
import django_tables2 as tables
from django_tables2.utils import A
from app.utils import ActionColumn

from .models import Project


class ProjectTable(tables.Table):
    title = tables.LinkColumn('view_project', args=[A('pk')], orderable=True)

    class Meta:
        model = Project
        template_name = 'django_tables2/bootstrap.html'
        fields = ('title', 'user')


class OwnProjectsTable(tables.Table):
    title = tables.LinkColumn('view_project', args=[A('pk')])
    edit = ActionColumn(verbose_name='', action='edit', accessor='id')
    delete = ActionColumn(verbose_name='', action='delete', accessor='id')

    class Meta:
        model = Project
        template_name = 'django_tables2/bootstrap.html'
        fields = ('id', 'title')
