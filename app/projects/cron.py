# -*- coding: utf-8 -*-
import datetime
from .models import ProjectViewsBlock

from django.core.mail import EmailMultiAlternatives


def send_unblocker_emails():
    now = datetime.datetime.now()
    blocks = ProjectViewsBlock.objects.filter(exceed_time__lte=now)
    for block in blocks:
        if block.projects.count() < 3:
            block.delete()
        user_email = block.user.email

        subject, from_email, to = 'You can view other Projects!', 'mail@limits.com', user_email
        text_content = 'This is an important message.'
        html_content = '<p>Blocking time exceed. You can see other projects!</p>'
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        block.expired = True
        block.save()