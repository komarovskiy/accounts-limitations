from django.db import models
from django.conf import settings
from model_utils.models import TimeStampedModel


class Project(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='projects', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self):
        return self.title


class SingletonModel(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


class ProjectViewLimits(SingletonModel):
    value = models.PositiveIntegerField(verbose_name="How many hours for limit projects views?", default=60)


class ProjectViewsBlock(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='blockings', on_delete=models.CASCADE)
    projects = models.ManyToManyField(Project)
    exceed_time = models.DateTimeField()
    expired = models.BooleanField(default=False)

    class Meta:
        ordering = ('-exceed_time',)
