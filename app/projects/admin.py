from django.contrib import admin

from .models import ProjectViewLimits


admin.site.register(ProjectViewLimits)
