# -*- coding: utf-8 -*-
from django.urls import path
from .views import UserProjectsView, UserProjectUpdateView, UserProjectDeleteView, UserProjectsCreateView, \
    ProjectDetailView, LimitsExceedView

urlpatterns = [
    path('limit_exceed/', LimitsExceedView.as_view(), name='limit_exceed'),
    path('own/', UserProjectsView.as_view(), name='own_projects'),
    path('add/', UserProjectsCreateView.as_view(), name='add_project'),
    path('<int:pk>', ProjectDetailView.as_view(), name='view_project'),
    path('<int:pk>/edit/', UserProjectUpdateView.as_view(), name='edit_project'),
    path('<int:pk>/delete/', UserProjectDeleteView.as_view(), name='delete_project')
]
